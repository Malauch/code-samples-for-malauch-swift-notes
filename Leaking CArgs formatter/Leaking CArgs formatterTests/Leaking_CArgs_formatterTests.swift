//
//  Leaking_CArgs_formatterTests.swift
//  Leaking CArgs formatterTests
//
//  Created by Michał Jurewicz on 28/12/2020.
//

import XCTest
@testable import Leaking_CArgs_formatter

class Leaking_CArgs_formatterTests: XCTestCase {
	lazy var tenThousandTimes = times(10_000)
	
	let formatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .none
		formatter.minimumIntegerDigits = 3
		formatter.maximumIntegerDigits = 3
		
		return formatter
	}()

	func times(_ repeats: Int) -> (() -> Void) -> Void {
		return { closure in
			for _ in 0...repeats {
				closure()
			}
		}
	}
	
	
	func testCArg() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = String(format: "%03d", int)
			}
		}
	}
	
	func testTwoCArgs() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = String(format: "%03d:%03d", int, int)
			}
		}
	}

	func testThreeCArgs() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = String(format: "%03d:%03d:%03d", int, int, int)
			}
		}
	}
	
	func testThreeSeparateCArgs() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = String(format: "%03d:", int)
				+ String(format: "%03d:", int)
				+ String(format: "%03d", int)
			}
		}
	}
	
	func testFormatter() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = formatter.string(for: int)!
			}
		}
	}
	
	func testFormatterTwoInts() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = formatter.string(for: int)! + formatter.string(for: int)!
			}
		}
	}
	
	func testFormatterThreeInts() throws {
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = formatter.string(for: int)! + formatter.string(for: int)! + formatter.string(for: int)!
			}
		}
	}
	
	func testCreateAndUseFormatter() throws {
		self.measure {
			let formatter = NumberFormatter()
			formatter.numberStyle = .none
			formatter.minimumIntegerDigits = 3
			formatter.maximumIntegerDigits = 3
			
			tenThousandTimes {
				let int = 5
				_ = formatter.string(for: int)! + formatter.string(for: int)! + formatter.string(for: int)!
			}
		}
	}
	
	func testUsingFormatterCreatedInLoop() throws {
		self.measure {
			tenThousandTimes {
				let formatter = NumberFormatter()
				formatter.numberStyle = .none
				formatter.minimumIntegerDigits = 3
				formatter.maximumIntegerDigits = 3
				
				let int = 5
				_ = formatter.string(for: int)! + formatter.string(for: int)! + formatter.string(for: int)!
			}
		}
	}
	
	func testUsingFormatterCreatedInComputedPropertie() throws {
		var formatter: NumberFormatter {
			let result = NumberFormatter()
			result.numberStyle = .none
			result.minimumIntegerDigits = 3
			result.maximumIntegerDigits = 3
			
			return result
		}
		
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = formatter.string(for: int)! + formatter.string(for: int)! + formatter.string(for: int)!
			}
		}
	}
	
	func testUsingFormatterCreatedInConfigStructWithComputedPropertie() throws {
		struct NumberFormatterConfig {
			let numberStyle: NumberFormatter.Style = .none
			let minimumIntegerDigits = 3
			let maximumIntegerDigits = 3
			
			var formatter: NumberFormatter {
				let result = NumberFormatter()
				result.numberStyle = numberStyle
				result.minimumIntegerDigits = minimumIntegerDigits
				result.maximumIntegerDigits = maximumIntegerDigits
				
				return result
			}
		}
		
		let config = NumberFormatterConfig()
				
		self.measure {
			tenThousandTimes {
				let int = 5
				_ = config.formatter.string(for: int)! + config.formatter.string(for: int)! + config.formatter.string(for: int)!
			}
		}
	}
	
}
