import SwiftUI
import  Combine

struct ContentView: View {
	var body: some View {
		NavigationView {
			VStack(spacing: 8) {
				NavigationLink("Number Formatter Example Example", destination: FormatterView())
				NavigationLink("2 CArgs Example", destination: TwoCArgsExample())
				NavigationLink("3 separate CArgs Example", destination: ThreeSeparateCArgsExample())
				NavigationLink("3 CArgs Example", destination: ThreeCArgsExample())
			}
		}
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}

struct ShowFormattedString: View {
	var formattedString: (Int) -> String
	@State private var int = 0
	
	var body: some View {
		Text(formattedString(int))
			.onTapGesture {
				int += 1
			}
	}
}


struct ThreeCArgsExample: View {
	var body: some View {
		ShowFormattedString(formattedString: formattedString)
	}
	
	func formattedString(int: Int) -> String {
		String(format: "%02d:%02d.%03d", int, int, int)
	}
}

struct ThreeSeparateCArgsExample: View {
	var body: some View {
		ShowFormattedString(formattedString: formattedString)
	}
	
	func formattedString(int: Int) -> String {
		String(format: "%02d:", int) +
		String(format: "%02d.", int) +
		String(format: "%03d", int)
	}
}

struct TwoCArgsExample: View {
	var body: some View {
		ShowFormattedString(formattedString: formattedString)
	}
	
	func formattedString(int: Int) -> String {
		String(format: "%02d:%02d", int, int)
	}
}

struct FormatterView: View {
	let twoDigitsFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .none
		formatter.minimumIntegerDigits = 2
		formatter.maximumIntegerDigits = 2
		return formatter
	}()
	
	let threeDigitsFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .none
		formatter.minimumIntegerDigits = 3
		formatter.maximumIntegerDigits = 3
		return formatter
	}()
		
	var body: some View {
		ShowFormattedString(formattedString: formattedString)
	}
	
	func formattedString(_ int: Int) -> String {
		let twoDigitsStr = twoDigitsFormatter.string(for: int)!
		let threeDigitsStr = threeDigitsFormatter.string(for: int)!
		
		return twoDigitsStr + ":" + twoDigitsStr + "." + threeDigitsStr
	}
}
